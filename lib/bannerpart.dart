import 'package:flutter/material.dart';
import 'package:flutter_list/constants/Sizeconfig.dart';
import 'package:google_fonts/google_fonts.dart';
import 'constants/constants.dart';

class BannerPart extends StatelessWidget {
  final width;
  final height;
  final String image;

  const BannerPart({Key key, this.width, this.height, this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    bool islandscape = SizeConfig.orientation == Orientation.landscape;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: Image.asset(
              image,
              height: islandscape ? height * 0.4 : height * 0.24,
              width: width,
              fit: BoxFit.fill,
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Container(
          width: width,
          child: Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 13.0),
            child: Column(
              children: [
                Text(
                    "Buy 'I DO' to a memroable experience buy i to a memmorable a mero experience",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: title.copyWith(fontSize: 18.0)),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                    "Need a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speech ",
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                    style: GoogleFonts.robotoCondensed(
                      fontSize: 16.0,
                      color: Colors.blueGrey,
                    )),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
