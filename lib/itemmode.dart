
class ItemModel {
  String image;
  ItemModel({
    this.image,
  });
}

List<ItemModel> itemlist = [
  ItemModel(image: "images/list/8.jfif"),
  ItemModel(image: "images/list/9.jfif"),
  ItemModel(image: "images/list/10.jpg"),
  ItemModel(image: "images/list/12.jfif"),
  ItemModel(image: "images/list/13.jpg"),
  ItemModel(image: "images/list/14.jpg"),
];

List<ItemModel> itemlist2 = [
  ItemModel(image: "images/list2/1.jpg"),
  ItemModel(image: "images/list2/2.jpg"),
  ItemModel(image: "images/list2/3.jpg"),
  ItemModel(image: "images/list2/4.jpg"),
  ItemModel(image: "images/list2/5.jpg"),
  ItemModel(image: "images/list2/6.jpg"),
];

List<ItemModel> itemlist3 = [
  ItemModel(image: "images/list3/5.jpg"),
  ItemModel(image: "images/list3/4.jpg"),
  ItemModel(image: "images/list3/6.jpg"),
  ItemModel(image: "images/list3/3.jpg"),
  ItemModel(image: "images/list3/2.jpg"),
  ItemModel(image: "images/list3/1.jpg"),
];

List<ItemModel> tourist = [
  ItemModel(image: "images/tourist/1.jpg"),
  ItemModel(image: "images/tourist/2.jpg"),
  ItemModel(image: "images/tourist/3.jpg"),
  ItemModel(image: "images/tourist/4.jfif"),
  ItemModel(image: "images/tourist/5.jpg"),
];

List<ItemModel> itemlist4 = [
  ItemModel(image: "images/list4/1.jpg"),
  ItemModel(image: "images/list4/2.jpg"),
  ItemModel(image: "images/list4/3.jpg"),
  ItemModel(image: "images/list4/4.jpg"),
  ItemModel(image: "images/list4/5.jpg"),
  ItemModel(image: "images/list4/6.jpg"),
];

List<ItemModel> itemlist5 = [
  ItemModel(image: "images/list4/3.jpg"),
  ItemModel(image: "images/list3/2.jpg"),
  ItemModel(image: "images/list4/3.jpg"),
  ItemModel(image: "images/list3/4.jpg"),
  ItemModel(image: "images/list4/5.jpg"),
  ItemModel(image: "images/list2/6.jpg"),
];
