import 'package:flutter/material.dart';
import 'package:flutter_list/constants/Sizeconfig.dart';
import 'package:flutter_list/detailpage.dart';
import 'package:flutter_list/itemmode.dart';
import 'package:google_fonts/google_fonts.dart';

class List1 extends StatefulWidget {
  final height;
  final width;
  final List<ItemModel> model;
  final int direction;
  const List1({Key key, this.height, this.width, this.model, this.direction})
      : super(key: key);
  @override
  ListState createState() {
    return ListState();
  }
}

class ListState extends State<List1> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final height = SizeConfig.screenHeight;
    final width = SizeConfig.screenWidth;
    bool islandscape = SizeConfig.orientation == Orientation.landscape;
    return GestureDetector(
      onTap: () {
         Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DetailPage()),
                );
      },
      child: new Container(
          height: islandscape ? height * 0.49 : height * 0.45,
          width: width,
          child: ListView.builder(
            itemCount: widget.model.length,
            shrinkWrap: true,
            padding: EdgeInsets.only(left: getProportionateScreenWidth(5.0)),
            scrollDirection:
                widget.direction == 0 ? Axis.horizontal : Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding:
                    EdgeInsets.only(left: getProportionateScreenWidth(12.0)),
                child: Container(
                  width: islandscape
                      ? width * 0.2
                      : getProportionateScreenWidth(200.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: islandscape ? height * 0.56 : height * 0.35,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          color: Colors.pink,
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(7.0),
                          child: Image.asset(
                            widget.model[index].image,
                            height: height * 0.22,
                            width: double.infinity,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text("Roberto Englund",
                          style: GoogleFonts.breeSerif(
                              fontWeight: FontWeight.w500,
                              fontSize: islandscape
                                  ? getProportionateScreenHeight(30.0)
                                  : getProportionateScreenHeight(16.0),
                              color: Colors.white)),
                      SizedBox(
                        height: 2.0,
                      ),
                      Text(
                        "Actor - Freedo cuson fire and all ",
                        maxLines: 1,
                        style: GoogleFonts.poppins(
                            fontSize: islandscape
                                ? getProportionateScreenHeight(26.0)
                                : getProportionateScreenHeight(13.0),
                            color: Colors.white),
                        overflow: TextOverflow.ellipsis,
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
    );
  }
}
