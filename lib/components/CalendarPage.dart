import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class CalendarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: SafeArea(
            child: Container(
      child: SfCalendar(
        view: CalendarView.month,
      ),
    )));
  }
}
