import 'package:flutter/material.dart';
import 'package:flutter_list/constants/Sizeconfig.dart';
import 'package:flutter_list/itemmode.dart';
import 'package:google_fonts/google_fonts.dart';

class VerticalList extends StatefulWidget {
  final height;
  final width;
  final List<ItemModel> model;
  const VerticalList({Key key, this.height, this.width, this.model})
      : super(key: key);
  @override
  VerticalListState createState() {
    return VerticalListState();
  }
}

class VerticalListState extends State<VerticalList> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final height = SizeConfig.screenHeight;
    final width = SizeConfig.screenWidth;
    bool islandscape = SizeConfig.orientation == Orientation.landscape;
    return Container(
      height: height,
      width: width,
      child: ListView.builder(
        itemCount: widget.model.length,
        shrinkWrap: true,
        padding: EdgeInsets.only(left: getProportionateScreenWidth(5.0)),
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(left: getProportionateScreenWidth(5.0),bottom:  getProportionateScreenWidth(10.0)),
            child: Container(
              width: double.infinity,
              height: 60.0,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 80.0,
                    width: 80.0,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      shape: BoxShape.circle,
                      image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: new NetworkImage(
                              "https://i.imgur.com/BoN9kdC.png")),
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Center(child: Text("You have a fan request",
                      style: GoogleFonts.breeSerif(
                          fontWeight: FontWeight.w500,
                          fontSize: islandscape
                              ? getProportionateScreenHeight(30.0)
                              : getProportionateScreenHeight(16.0),
                          color: Colors.white))),
                  SizedBox(
                    height: 2.0,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
