import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_list/components/NotificationsPage.dart';
import 'package:flutter_list/components/UserList.dart';

class TabNavigationItem {
  final Widget page;
  final String title;
  final Icon icon;

  TabNavigationItem({
    @required this.page,
    @required this.title,
    @required this.icon,
  });
  
  static List<TabNavigationItem> get items => [
        TabNavigationItem(
          page: UserList(),
          icon: Icon(Icons.home),
          title: "",
        ),
        TabNavigationItem(
          page: NotificationsPage(),
          icon: Icon(Icons.message_rounded),
          title: "",
        ),
        TabNavigationItem(
          page: UserList(),
          icon: Icon(Icons.notifications),
          title: "",
        ),
        TabNavigationItem(
          page: UserList(),
          icon: Icon(Icons.person),
          title: "",
        ),
      ];
}