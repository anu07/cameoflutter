import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_list/components/footer_login.dart';
import 'package:flutter_list/components/login_fresh.dart';
import 'package:flutter_list/components/login_fresh_reset_password.dart';
import 'package:flutter_list/components/login_fresh_sign_up.dart';
import 'package:flutter_list/components/login_user_password.dart';
import 'package:flutter_list/service/sign_up_model.dart';
import 'package:flutter_list/service/type_login.dart';

class SplashPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 5),
        () => Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => buildLoginFresh())));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: Center(
          child: Text('Vint',
              style: TextStyle(
                decoration: TextDecoration.none,
                  fontSize: 36.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white))),
    );
  }

  Widget widgetFooter() {
    return LoginFreshFooter(
      logo: '',
      text: '',
      funFooterLogin: () {
        // develop what they want the footer to do when the user clicks
      },
    );
  }

  LoginFresh buildLoginFresh() {  
  List<LoginFreshTypeLoginModel> listLogin = [  
    LoginFreshTypeLoginModel(  
        callFunction: (BuildContext _buildContext) {  
          // develop what they want the facebook to do when the user clicks  
  },  
        logo: TypeLogo.facebook),  
    LoginFreshTypeLoginModel(  
        callFunction: (BuildContext _buildContext) {  
          // develop what they want the Google to do when the user clicks  
  },  
        logo: TypeLogo.google),  
    LoginFreshTypeLoginModel(  
        callFunction: (BuildContext _buildContext) {  
          print("APPLE");  
          // develop what they want the Apple to do when the user clicks  
  },  
        logo: TypeLogo.apple),  
    LoginFreshTypeLoginModel(  
        callFunction: (BuildContext _buildContext) {  
          Navigator.of(_buildContext).push(MaterialPageRoute(  
            builder: (_buildContext) => widgetLoginFreshUserAndPassword(),  
          ));  
        },  
        logo: TypeLogo.userPassword),  
  ];  
  return LoginFresh(  
          pathLogo: 'assets/logo.jpg',  
          isExploreApp: true,  
          functionExploreApp: () {  
            // develop what they want the ExploreApp to do when the user clicks  
  },  
          isFooter: false,  
          widgetFooter: null,  
          typeLoginModel: listLogin,  
          isSignUp: true,  
          widgetSignUp: this.widgetLoginFreshSignUp(),  
    );  
}




  Widget widgetLoginFreshSignUp() {
    return LoginFreshSignUp(
        isFooter: false,
        widgetFooter: null,
        logo: 'assets/logo.jpg',
        funSignUp: (BuildContext _context, Function isRequest,
            SignUpModel signUpModel) {
          isRequest(true);

          print(signUpModel.email);
          print(signUpModel.password);
          print(signUpModel.repeatPassword);
          print(signUpModel.surname);
          print(signUpModel.name);

          isRequest(false);
        });
  }

  Widget widgetLoginFreshUserAndPassword() {
    return LoginFreshUserAndPassword(
      callLogin: (BuildContext _context, Function isRequest, String user,
          String password) {
        isRequest(true);

        Future.delayed(Duration(seconds: 2), () {
          print('-------------- function call----------------');
          print(user);
          print(password);
          print('--------------   end call   ----------------');

          isRequest(false);
        });
      },
      logo: 'assets/logo.jpg',
      isFooter: false,
      widgetFooter: null,
      isResetPassword: true,
      widgetResetPassword: this.widgetResetPassword(),
      isSignUp: true,
      signUp: this.widgetLoginFreshSignUp(),
    );
  }

  Widget widgetResetPassword() {
    return LoginFreshResetPassword(
      logo: 'assets/logo.png',
      funResetPassword:
          (BuildContext _context, Function isRequest, String email) {
        isRequest(true);

        Future.delayed(Duration(seconds: 2), () {
          print('-------------- function call----------------');
          print(email);
          print('--------------   end call   ----------------');
          isRequest(false);
        });
      },
      isFooter: false,
      widgetFooter: null,
    );
  }
}
