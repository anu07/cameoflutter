import 'package:flutter/material.dart';
import 'package:flutter_list/constants/Sizeconfig.dart';
import 'package:flutter_list/detailpage.dart';
import 'package:flutter_list/itemmode.dart';

class List2 extends StatelessWidget {
  final height;
  final width;

  const List2({Key key, this.height, this.width}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    bool islandscape = SizeConfig.orientation == Orientation.landscape;
    return Container(
      height: islandscape ? height * 0.35 : height * 0.23,
      width: width,
      child: Padding(
        padding: EdgeInsets.only(
            top: getProportionateScreenHeight(5.0),
            bottom: getProportionateScreenHeight(5.0)),
        child: ListView.builder(
          itemCount: itemlist3.length,
          padding: EdgeInsets.only(left: getProportionateScreenWidth(5.0)),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.only(left: getProportionateScreenWidth(8.0)),
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return DetailPage();
                  }));
                },
                child: Container(
                  width: islandscape ? width * 0.17 : width * 0.32,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: Colors.pink),
                  child: Stack(
                    children: [
                      Positioned(
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(5.0),
                            child: Image.asset(
                              itemlist3[index].image,
                              height: height * 0.23,
                              width: width * 0.32,
                              fit: BoxFit.fill,
                            )),
                      ),
                      Positioned(
                          child: Container(
                        height: height * 0.23,
                        width: width * 0.32,
                        color: Colors.white.withOpacity(0.3),
                      )),
                      Positioned(
                          bottom: getProportionateScreenHeight(10.0),
                          left: 0,
                          right: 0,
                          child: Column(
                            children: [
                              Container(
                                height: islandscape
                                    ? getProportionateScreenHeight(110.0)
                                    : getProportionateScreenHeight(50.0),
                                width: islandscape
                                    ? getProportionateScreenWidth(28.0)
                                    : getProportionateScreenWidth(50.0),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle, color: Colors.red),
                                child: ClipRRect(
                                    borderRadius: islandscape
                                        ? BorderRadius.circular(100)
                                        : BorderRadius.circular(50.0),
                                    child: Image.asset(
                                      itemlist2[index].image,
                                      fit: BoxFit.fill,
                                    )),
                              ),
                              SizedBox(
                                height: getProportionateScreenHeight(5.0),
                              ),
                              Text(
                                "Andrew",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ))
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
