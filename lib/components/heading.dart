import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Heading extends StatelessWidget {
  final title;
  final String description;
  final cpadding;

  const Heading({Key key, this.title, this.description, this.cpadding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: cpadding, right: cpadding),
      child: Row(
        children: [
          Text(title,
              style: GoogleFonts.sourceSansPro(
                  fontSize: 20.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold)),
          Spacer(),
          Row(
            children: [
              Text(
                "See all",
                style: TextStyle(color: Colors.white, fontSize: 15.0),
              ),
              SizedBox(
                width: 4.0,
              ),
              Icon(
                Icons.arrow_forward,
                size: 13,
              )
            ],
          ),
        ],
      ),
    );
  }
}
