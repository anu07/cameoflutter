import 'package:flutter/material.dart';
import 'package:flutter_list/constants/Sizeconfig.dart';
import 'package:flutter_list/itemmode.dart';

class List3 extends StatelessWidget {
  final height;
  final width;

  const List3({Key key, this.height, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    bool islandscape = SizeConfig.orientation == Orientation.landscape;
    return Container(
      height: islandscape ? height * 0.3 : height * 0.18,
      width: width,
      child: Padding(
        padding: EdgeInsets.only(
            top: getProportionateScreenHeight(5.0),
            bottom: getProportionateScreenHeight(5.0)),
        child: ListView.builder(
          itemCount: tourist.length,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.only(left: getProportionateScreenWidth(5.0)),
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.only(left: getProportionateScreenWidth(8.0)),
              child: Container(
                width: islandscape ? width * 0.34 : width * 0.6,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  color: Colors.pink,
                ),
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.asset(
                        tourist[index].image,
                        width: islandscape ? width * 0.34 : width * 0.6,
                        height: islandscape ? height * 0.3 : height * 0.18,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Positioned(
                        child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.white.withOpacity(0.3),
                      ),
                      height: height * 0.18,
                      width: width * 0.6,
                    ))
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
