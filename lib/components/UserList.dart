import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_list/components/DataSearch.dart';
import 'package:flutter_list/components/ProfilePage.dart';
import 'package:flutter_list/components/heading.dart';
import 'package:flutter_list/components/list1.dart';
import 'package:flutter_list/constants/Sizeconfig.dart';
import 'package:flutter_list/itemmode.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_list/model/ListData.dart';
import '../itemmode.dart';

class UserList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final height = SizeConfig.screenHeight;
    final width = SizeConfig.screenWidth;

    const double cpadding = 14.0;
    const double spacing = 10.0;
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('Browse',
            style: TextStyle(
                fontSize: 36.0,
                fontWeight: FontWeight.bold,
                color: Colors.white)),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                showSearch(context: context, delegate: DataSearch(listWords));
              }),
          IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfilePage()),
                );
              },
              icon: Icon(Icons.person))
        ],
      ),
      body: Center(
        child: ListView(
          children: [
            SizedBox(
              height: 20.0,
            ),
            Heading(
              title: "Featured",
              cpadding: cpadding,
            ),
            SizedBox(
              height: 10.0,
            ),
            List1(
              height: height,
              width: width,
              model: itemlist2,
              direction: 0,
            ),
           
            Heading(
              title: "Yoga",
              cpadding: cpadding,
            ),
            SizedBox(
              height: 10.0,
            ),
            List1(height: height, width: width, model: itemlist3, direction: 0),
            SizedBox(
              height: spacing,
            ),
            Heading(
              title: "Arts & Craft",
              cpadding: cpadding,
            ),
            SizedBox(
              height: 10.0,
            ),
            List1(
              height: height,
              width: width,
              model: itemlist4,
              direction: 0,
            ),
            SizedBox(
              height: spacing,
            ),
            Heading(
              title: "Dance",
              cpadding: cpadding,
            ),
            SizedBox(
              height: 10.0,
            ),
            List1(
              height: height,
              width: width,
              model: itemlist5,
              direction: 0,
            ),
            SizedBox(
              height: spacing,
            ),
            /*   BannerPart(
                image: "images/green.jpg",
                height: height,
                width: width,
              ),
              SizedBox(
                height: spacing,
              ),*/
            Heading(
              title: "Acting",
              cpadding: cpadding,
            ),
            SizedBox(
              height: 10.0,
            ),
            List1(
              height: height,
              width: width,
              model: itemlist,
              direction: 0,
            ),
            SizedBox(
              height: spacing,
            ),
            Heading(
              title: "Science and technology",
              cpadding: cpadding,
            ),
            SizedBox(
              height: 10.0,
            ),
            List1(
              height: height,
              width: width,
              model: itemlist2,
              direction: 0,
            ),
            SizedBox(
              height: spacing,
            )
          ],
        ),
      ),
    );
  }
}
