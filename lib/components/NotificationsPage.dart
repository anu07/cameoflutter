import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_list/components/VerticalList.dart';
import 'package:flutter_list/constants/Sizeconfig.dart';
import 'package:flutter_list/itemmode.dart';

class NotificationsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final height = SizeConfig.screenHeight;
    final width = SizeConfig.screenWidth;

    bool islandscape = SizeConfig.orientation == Orientation.landscape;
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
          child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 28, right: 28, top: 16.0),
            child: Container(
              height: islandscape
                  ? getProportionateScreenHeight(80)
                  : getProportionateScreenHeight(40),
              width: width * 0.8,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
                // border: Border.all(width: 0.6, color: Colors.blueGrey)
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                child: Row(
                  children: [
                    Text(
                      "Notifications",
                      style: TextStyle(
                          fontSize: 36.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 40.0,
          ),
          VerticalList(
            height: height,
            width: width,
            model: itemlist2,
          ),
        ],
      )),
    );
  }
}
