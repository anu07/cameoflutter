List<ListWords>  listWords = [
  ListWords('Andy Buckley', 'OneWord definition'),
  ListWords('Ammine Leo', 'TwoWord definition.'),
  ListWords('Anna Bee', 'TreeWord definition'),
];

class ListWords {
  String titlelist;
  String definitionlist;

  ListWords(String titlelist, String definitionlist) {
    this.titlelist = titlelist;
    this.definitionlist = definitionlist;
  }
}