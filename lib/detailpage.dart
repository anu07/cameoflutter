import 'package:flutter/material.dart';
import 'package:flutter_list/constants/Sizeconfig.dart';
import 'package:flutter_list/constants/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final height = SizeConfig.screenHeight;
    final width = SizeConfig.screenWidth;

    return Scaffold(
      backgroundColor: Colors.black,
       appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text("Andy Buckley"),
        centerTitle: false,
      ),
      body: Container(
        height: height,
        width: double.infinity,
        child: Stack(
          children: [
            ListView(
              children: [
                SizedBox(
                  height: getProportionateScreenHeight(20.0),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(7.0),
                  child: Image.asset(
                    "images/list3/1.jpg",
                    height: height * 0.33,
                    fit: BoxFit.fitHeight,
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenWidth(15.0),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: getProportionateScreenWidth(13.0),
                      top: getProportionateScreenHeight(10.0)),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Andy Buckley",
                          style: title.copyWith(
                              fontSize: getProportionateScreenHeight(22.0)),
                        ),
                         SizedBox(
                          height: getProportionateScreenHeight(20.0),
                        ),
                        Text(
                          "Actor - The official actor of america",
                          style: description,
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(20.0),
                        ),
                        Container(
                          child: Text(
                              "Need a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speech e? How about a joke forr"
                              "Need a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speecNeed a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speech e? How about a joke forr"
                              "Need a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speech  couple? How about a joke for the best man speech e? How about a joke forr"
                              "Need a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speech",
                              style: GoogleFonts.robotoCondensed(
                                fontSize: getProportionateScreenHeight(16.0),
                                color: Colors.white,
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(70.0),
                )
              ],
            ),
            Positioned(
                bottom: 0,
                child: Container(
                  height: getProportionateScreenHeight(60),
                  width: width,
                  color: Colors.black,
                  child: Row(
                    children: [
                      Spacer(),
                      Padding(
                        padding: EdgeInsets.only(
                            top: getProportionateScreenHeight(5.0),
                            bottom: getProportionateScreenWidth(4.0)),
                            // right: getProportionateScreenWidth(13.0)),
                        child: Container(
                          height: height,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            color: Colors.black,
                          ),
                          width: double.infinity,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                onPressed: () {},
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  elevation: 15.0,
                                ),
                                child: Container(
                                  constraints: BoxConstraints(
                                    maxWidth: 100.0,
                                    maxHeight: 40.0,
                                  ),
                                  alignment: Alignment.center,
                                  child: Text(
                                    "Call",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12.0,
                                        letterSpacing: 2.0,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ),
                              ),
                              SizedBox(width: 30,),
                              ElevatedButton(
                                onPressed: () {},
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  elevation: 15.0,
                                ),
                                child: Container(
                                  constraints: BoxConstraints(
                                    maxWidth: 100.0,
                                    maxHeight: 40.0,
                                  ),
                                  alignment: Alignment.center,
                                  child: Text(
                                    "Message",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12.0,
                                        letterSpacing: 2.0,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
