import 'package:flutter/material.dart';
import 'package:flutter_list/constants/Sizeconfig.dart';
import 'package:google_fonts/google_fonts.dart';

final TextStyle heading1 = GoogleFonts.sourceSansPro(
    fontSize: getProportionateScreenHeight(22.0),
    color: Colors.white,
    fontWeight: FontWeight.bold);

final TextStyle title = GoogleFonts.robotoCondensed(
    fontWeight: FontWeight.w600,
    fontSize: getProportionateScreenHeight(16.0),
    color: Colors.blueGrey[500]);

final description = GoogleFonts.poppins(
    fontSize: getProportionateScreenHeight(13.0), color: Colors.grey[300]);
