class TypeLogo {
  static String facebook = 'assets/images/facebook.png';
  static String google = 'assets/images/google.png';
  static String apple = 'assets/images/apple.png';
  static String userPassword = 'assets/images/user_password.png';
}

class LoginFreshTypeLoginModel {
  Function callFunction;
  String logo;

  LoginFreshTypeLoginModel({this.logo, this.callFunction});
}
