import 'package:flutter/material.dart';

import 'package:flutter_list/constants/Sizeconfig.dart';
import 'package:flutter_list/constants/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class Detail2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final height = SizeConfig.screenHeight;
    final width = SizeConfig.screenWidth;

    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        height: height,
        width: width,
        child: Stack(
          children: [
            ListView(
              children: [
                Container(
                  height: 60,
                  width: width,
                  color: Colors.green,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 14.0, top: 10.0),
                    child: Text(
                      "Join Fan Club",
                      style: TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(7.0),
                  child: Image.asset(
                    "images/list3/1.jpg",
                    height: height * 0.33,
                    fit: BoxFit.fitHeight,
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 13.0, top: 10.0),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Andy Buckley",
                          style: title.copyWith(fontSize: 22.0),
                        ),
                        Text(
                          "Actor - The official actor of america",
                          style: description,
                        ),
                        SizedBox(
                          height: 50.0,
                        ),
                        Container(
                          child: Text(
                              "Need a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speech e? How about a joke forr"
                              "Need a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speecNeed a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speech e? How about a joke forr"
                              "Need a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speech  couple? How about a joke for the best man speech e? How about a joke forr"
                              "Need a gift for the happy couple? How about a joke for the best man speech ? Or even getting your bestie that something 'new' we got the stars just for itNeed a gift for the happy couple? How about a joke for the best man speech",
                              style: GoogleFonts.robotoCondensed(
                                fontSize: 16.0,
                                color: Colors.white,
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
